const locations = 
[
    [50.484405, 30.605973],
    [50.484334, 30.605994],
    [50.484265, 30.606026],
    [50.484189, 30.606034],
    [50.484133, 30.605998],
]
// [
//     [50.484405, 30.605973],
//     [50.484363, 30.605981],
//     [50.484334, 30.605994],
//     [50.484299, 30.606005],
//     [50.484265, 30.606026],
//     [50.484227, 30.606038],
//     [50.484189, 30.606034],
//     [50.484153, 30.606020],
//     [50.484133, 30.605998],
//     [50.484110, 30.605979]
// ]

let iterator;

exports.getCurrent = () => {
    if (iterator == null || iterator == locations.length) {
        iterator = 0;
    }
    let location = locations[iterator];
    iterator++;
    return location;
}

exports.getCityId = () => {
    return 1;
}