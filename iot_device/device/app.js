const Location = require('./location');
const Tourist = require('./tourist');
const fetch = require("node-fetch");
const baseUrl = 'http://localhost:7070'

const meterPerDegree = 111000;
const timePeriodSec = 10;
const normalSpeed = 1.4;

const speed = async () => {
    let location1 = Location.getCurrent();
    await sleep(timePeriodSec * 1000);
    let location2 = Location.getCurrent();

    let y = Math.abs(location2[0] - location1[0]) * meterPerDegree;
    let x = Math.abs(location2[1] - location1[1]) * meterPerDegree;
    let distance = Math.sqrt(x*x + y*y);
    let speed = distance / timePeriodSec; //meter per sec
    
    return speed;
}

const checkPlacesNearby = async () => {
    let cityId = Location.getCityId();
    let location = Location.getCurrent();
    let touristId = Tourist.getId();
    let durationLong = await isDurationLong;
    let url = baseUrl + '/places/' + location[0] + '/' + location[1] 
            + '/' + cityId + '/' + touristId + '/' + durationLong;
    
    fetch(url, {
        headers: { "Content-type": "application/json" },
        method: "GET" 
    }).then((res) => {
        if (res.ok) {
            res.json()
            .then( (data) => {
                let message;
                
                if (data.length > 0) {
                    let placeId = data[0].id;
                    let duration = durationLong? "long" : "short";
                    message = "You are near by delightful place!\n*Play place #"
                        + placeId + " audio, duration " + duration + "*";
                } else {
                    message = "There is no places nearby."
                }
                notifyUser(message);
            });
        } else {
            console.log("Error!");
        }
    })
    .catch(err => console.log(err));
}

const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

const isDurationLong = new Promise ((resolve, reject) => {
    speed()
    .then(res => {
        if(res > normalSpeed) {
            resolve(false);
        } else {
            resolve(true);
        }   
    });
});

const notifyUser = (message) => {
    console.log("\nNotification\n" + message + "\n");
}

checkPlacesNearby();

module.exports = Location;