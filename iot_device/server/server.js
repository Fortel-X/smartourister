const http = require('http');
// const app = require('./app');
const fetch = require("node-fetch");
const url = 'https://smartourister.herokuapp.com';
const express = require('express');
const server = express();
const port = process.env.PORT || 7070;
// const server = http.createServer(app);

let getPlacesInTheCity = (cityId) => {
    return new Promise((resolve, reject) => {
        fetch(url + '/places/city/' + cityId, {
            headers: {
                "Content-type": "application/json",
                },
            method: "GET"
        }).then(res => {
            if (res.ok) {
                res.json().then((data) => {
                    resolve(data);
                });
            } else {
                console.log("Error!");
                reject({"message": "error"});
            }
        }).catch((err) => { console.log(err); });
  });
};

const chooseNearby = (places, currentLatitude, currentLongitude) => {
    const marginOk = 0.0005; // 0.0001 degree == 11 meters
    let placesNearby = [];

    places.forEach(place => {
        let dY = Math.abs(place.latitude - currentLatitude);
        let dX = Math.abs(place.longitude - currentLongitude);
        if(dY < marginOk && dX < marginOk) {
            placesNearby.push(place);
        }
    });
    return placesNearby;
}

const visitPlace = (placeId, touristId, date, durationLong) => {
    data = {
        "place_id": placeId,
        "tourist_id": touristId,
        "date": date,
        "duration_long": durationLong
    };

    fetch(url + "/visited_places/add",{
        headers: {
            "Content-type": "application/json",
            },
        method: "POST",
        body: JSON.stringify(data)
    }).then(res => {
        if (!res.ok) {
            console.log("Error!");
        }
    }).catch((err) => { console.log(err); });
}

const getDate = () => {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    today =  yyyy + '-' + mm + '-' + dd;
    return today;
}

const findPlacesNearby = async (req, res) => {
    const cityId = req.params.cityId;
    const latitude = req.params.latitude;
    const longitude = req.params.longitude;
    const touristId = req.params.touristId;
    const durationLong = req.params.durationLong;

    let places = await getPlacesInTheCity(cityId)
        .catch((err) => { console.log(err); });

    let placesNearby = chooseNearby(places, latitude, longitude);

    if(placesNearby.length > 0) {
        visitPlace(placesNearby[0].id, touristId, getDate(), durationLong);
    }

    res.status(200).json(placesNearby);
}

server.get('/places/:latitude/:longitude/:cityId/:touristId/:durationLong', findPlacesNearby);

server.listen(port, function(){
    console.log("Server is running on port " + port);
});