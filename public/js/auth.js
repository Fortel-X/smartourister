let url = "https://smartourister.herokuapp.com";
//var url = "http://localhost:7000";

function loginSubmit() {
	let form = document.getElementById('loginForm');

	if(!emailValidation(form) || !passwordValidation(form)) {
		return;
	}

	let newUrl = url + "/guides/login";
	let email = form.email.value;
	let password = form.password.value;

	let nextPage = (email == "admin@admin.com") ? "admin.html" : "main.html";

	fetch(newUrl, {
		method: "POST",
    	headers: { "Content-type": "application/json" },
		body: JSON.stringify({email: email, password: password})
  	}).then((res) => {
		if (res.ok) {
			res.json().then((data) => {
				localStorage.setItem('token', data.token);
				localStorage.setItem('id', data.id);
				document.location.href = nextPage;
			});
		} else {
			alert("Error!");
    	}
  });
    
}

function signupSubmit() {
	let form = document.getElementById('signupForm');

	if(!nameValidation(form) || !emailValidation(form) || !passwordValidation(form)) {
		return;
	}

	let newUrl = url + "/guides/signup";
	let name = form.name.value;
	let email = form.email.value;
	let password = form.password.value;
	fetch(newUrl, {
		method: "POST",
    	headers: { "Content-type": "application/json" },
		body: JSON.stringify({email: email, password: password, name: name})
  	}).then((res) => {
		if (res.ok) {
			res.json().then((data) => {
				localStorage.setItem('token', data.token);
				localStorage.setItem('id', data.id);
				document.location.href = "main.html";
			});
		} else {
			alert("Error!");
    	}
  });
}

function nameValidation(form) {
	let valid = true;
	if (document.getElementById("name").value == '') {
		
		if(localStorage.getItem('local') == 'UA'){
			document.getElementById("nameHelp").innerText = "Ім'я не має містити спеціальних знаків та має складатись хоча б з одного символа.";
		} else if(localStorage.getItem('local') == 'EN'){
			document.getElementById("nameHelp").innerText = "The name must not contain special characters and must consist of at least one character.";
		}
		form.name.style.borderColor = '#CC3E37';
		valid = false;
	} else {
		form.email.style.borderColor = '#d4edda';
		document.getElementById("nameHelp").innerText ='';
	}
	return valid;
}

function emailValidation(form) {
	var valid = true;
	if (form.email.value.match('\^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$') == null) {
		if(localStorage.getItem('local') == 'UA'){
			document.getElementById("emailHelp").innerText = "Введіть дійсну електронну пошту.";
		} else if(localStorage.getItem('local') == 'EN'){
			document.getElementById("emailHelp").innerText = "Enter a valid email.";
		}
		form.email.style.borderColor = '#CC3E37';
		valid = false;
	} else {
		form.email.style.borderColor = '#d4edda';
		document.getElementById("emailHelp").innerText ='';
	}
	return valid;
}

function passwordValidation(form){
	var valid = true;
	if (form.password.value.match('\^([0-9a-zA-Z!@#$%^&*]{8,})$') == null) {
		if(localStorage.getItem('local') == 'UA'){
			document.getElementById("passwordHelp").innerText = "Ваш пароль повинен містити не менше 8 символів. Будь ласка спробуйте ще раз.";
		} else if(localStorage.getItem('local') == 'EN'){
			document.getElementById("passwordHelp").innerText = "Your password must be at least 8 characters. Please try again.";
		}
		form.password.style.borderColor = '#CC3E37';
		valid = false;
	} else {
		form.password.style.borderColor = '#d4edda';
		document.getElementById("passwordHelp").innerText ='';
	}
	return valid;
}