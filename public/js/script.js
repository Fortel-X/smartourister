function logOut() {
    localStorage.token = null;
    localStorage.id = null;
    window.location = "index.html?#";
}

let getCities = () => {
    return new Promise((resolve, reject) => {
        fetch(baseUrl + '/cities/', {
            headers: {
                "Content-type": "application/json",
                },
            method: "GET"
        }).then(res => {
            if (res.ok) {
                res.json().then((data) => {
                    resolve(data.cities);
                });
            } else {
                console.log("Error!");
                reject({"message": "error"});
            }
        }).catch((err) => { console.log(err); });
  });
};