let gMap;
let mapMarkers = [];
let currentMarker;

function setMap (map) {
  gMap = map; 
  gMap.addListener('click', function(event) {
    clearMarkers();
    addMarker(event.latLng);
  });
}

// Adds a marker to the map 
function addMarker(location) {
  currentMarker = new google.maps.Marker({
    position: location,
    map: gMap
  });
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < mapMarkers.length; i++) {
    mapMarkers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}



// Shows any markers currently in the array.
// function showMarkers() {
//   setMapOnAll(map);
// }

// Deletes all markers in the array by removing references to them.
// function deleteMarkers() {
//   clearMarkers();
//   markers = [];
// }