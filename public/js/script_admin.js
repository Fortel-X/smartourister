let baseUrl = "https://smartourister.herokuapp.com";

let editPlace = async (placeId) => {
    let placeCard = document.getElementById("placeCard" + placeId);

    let placeName = document.getElementById("name" + placeId).innerText;

    let htmlString = `<div class="card my-card-admin" id="placeCard${placeId}">` +
        `<h5 class="card-header" id="name${placeId}"><input type="text" value="${placeName}" id="name-input${placeId}" style="color: #031926"></h5>` +
        `<div class="card-body">` +
          `<div>` +
            `<br/><a href="#" class="btn btn-primary my-button" id="button${placeId}" onClick="updatePlace(${placeId})">Save</a>` +
          `</div>` +
        `</div>` +
      `</div>`;

    placeCard.outerHTML = htmlString;
}

let updatePlace = async (placeId) => {
    let token = localStorage.token;
    let url = baseUrl + '/places/' + placeId;

    let name = document.getElementById("name-input" + placeId).value;

    let response = await fetch(url, { 
        method: 'PATCH',
        body: JSON.stringify({ name: name }),
        headers: { 'Content-Type':  'text/plain',
                    'Authorization': 'Bearer: ' + token }
    })
    .catch(error => {
        alert(error.message);
    });

    let result = await response.json().catch(error => alert(error.message));
    console.log(result);
    fillPlacesListAdmin();
}

async function fillPlacesListAdmin() {
    let token = localStorage.token;

    let url = baseUrl + '/places/';

    let response = await fetch(url, { 
        method: 'GET',
        headers: { 'Content-Type': 'application/json;charset=utf-8',
                    'Authorization': 'Bearer: ' + token }
    })
    .catch(error => {
        alert(error.message);
    });

    let result = await response.json().catch(error => alert(error.message));
    console.log(result);
    var placeList = result;
    var cardsholder = document.getElementById("cardsholder_admin");
    
    var listSize = placeList.length;
    var htmlString = ``;

    for (i = 0; i < listSize; i++) {
        let id = placeList[i].id;
        let placeName = placeList[i].place_name;
        let cityName = placeList[i].city_name;
        let latitude = placeList[i].latitude;
        let longitude = placeList[i].longitude;
        let visits = placeList[i].visits;
        let long_visits = placeList[i].long_visits;
        let statistics_visits = ``;
        let statistics_long_visits = ``;
        
        if (visits == null) {
            if (localStorage.getItem('local') == 'EN') {
                statistics_visits = `This place was not visited yet.`;
            } else if (localStorage.getItem('local') == 'UA') {
                statistics_visits = `Це місце ще не відвідав жоден турист.`;
            }
        } else {
            if (localStorage.getItem('local') == 'EN') {
                statistics_visits = `This place was visited ${visits} time(s)`
            } else if (localStorage.getItem('local') == 'UA') {
                statistics_visits = `Це місце було відвідано ${visits} раз(ів).`;
            }
            if (long_visits == null) {
                if (localStorage.getItem('local') == 'EN') {
                    statistics_visits = `But there was no one long visit.`;
                } else if (localStorage.getItem('local') == 'UA') {
                    statistics_visits = `Проте жоден турист ще не прослухав повну екскурсію.`;
                }
            } else {
                if (localStorage.getItem('local') == 'EN') {
                    statistics_long_visits = `And there was ${long_visits} long visit(s).`;
                } else if (localStorage.getItem('local') == 'UA') {
                    statistics_long_visits = `Повну екскурсію було прослухано ${long_visits} раз(ів).`;
                }
            }
        }

        let currentString = `<div class="card my-card-admin" id="placeCard${id}">` +
        `<h5 class="card-header" id="name${id}">${placeName}</h5>` +
        `<div class="card-body">` +
          `<div style="float:left">` +
            `<p class="card-text my-city-label">City: <text id="city${id}">${cityName}</text></p>` +
            `<a href="#" class="btn btn-primary my-button" id="button${id}" onClick="editPlace(${id})">Edit place</a>` +
          `</div>` +
          `<div id="statistics${id}" style="float:right">` +
            `<p class="card-text my-statistics-label">Statistics:</p>` +
            `<p class="card-text">${statistics_visits}</p>` +
            `<p class="card-text">${statistics_long_visits}</p>` +
          `</div>` +
        `</div>` +
      `</div>`;

        htmlString += currentString;
    }

    cardsholder.innerHTML = htmlString;
}