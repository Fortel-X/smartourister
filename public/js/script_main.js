let baseUrl = "https://smartourister.herokuapp.com";

let loadPage = () => fillPlacesList();

let editPlace = async (placeId) => {
    let placeCard = document.getElementById("placeCard" + placeId);

    let placeName = document.getElementById("name" + placeId).innerText;

    let htmlString = `<div class="card my-card" id="placeCard${placeId}">` +
        `<h5 class="card-header" id="name${placeId}"><input type="text" value="${placeName}" id="name-input${placeId}" style="color: #031926"></h5>` +
        `<div class="card-body">` +
          `<div>` +
            `<br/><a href="#" class="btn btn-primary my-button" id="button${placeId}" onClick="updatePlace(${placeId})">Save</a>` +
          `</div>` +
        `</div>` +
      `</div>`;

    placeCard.outerHTML = htmlString;
}

let createPlace = async () => {
    let placeCard = document.getElementById("new_place");

    let cities = await getCities();
    let selectCity = "";

    console.log(cities);

    for (let i = 0; i < cities.length; i++) {
        selectCity += `<option value="${cities[i].id}" selected="selected">${cities[i].name}</option>`
    }
    console.log(selectCity);

    let htmlString = `<div class="card my-card" id="placeCard">` +
        `<h5 class="card-header" id="name">Name: <input type="text" id="name-input" style="color: #031926"></h5>` +
        `<div class="card-body">` +
            `<div>` +
            `<select name="city" size="4" id="select">${selectCity}</select>`+
            `</div>` +
          `<div>` +
            `<br/><a href="#" class="btn btn-primary my-button" id="button_create" onClick="addPlace()">Create</a>` +
          `</div>` +
        `</div>` +
      `</div>`;

    placeCard.outerHTML = htmlString;

    clearMarkers();
}

let updatePlace = async (placeId) => {
    let token = localStorage.token;
    let url = baseUrl + '/places/' + placeId;

    let name = document.getElementById("name-input" + placeId).value;

    let response = await fetch(url, { 
        method: 'PATCH',
        body: JSON.stringify({ name: name }),
        headers: { 'Content-Type':  'text/plain',
                    'Authorization': 'Bearer: ' + token }
    })
    .catch(error => {
        alert(error.message);
    });

    let result = await response.json().catch(error => alert(error.message));
    console.log(result);
    fillPlacesList();
}

let addPlace = async () => {
    let token = localStorage.token;
    let url = baseUrl + '/places/add';

    console.log(currentMarker.position.lat());

    let name = document.getElementById("name-input").value;
    let city_id = document.getElementById("select").value;
    let short_audio = "0";
    let detailed_audio = "0";
    let latitude = currentMarker.position.lat();
    let longitude = currentMarker.position.lng();
    let guide_id = localStorage.id;

    let bodyObject = {
        latitude: latitude,
        longitude: longitude,
        short_audio: short_audio,
        detailed_audio: detailed_audio,
        city_id: city_id,
        guide_id: guide_id,
        name: name
    }

    console.log(bodyObject);

    fetch(url, {
		method: "POST",
    	headers: { "Content-type": "application/json" },
		body: JSON.stringify(bodyObject)
  	}).then((res) => {
		if (res.ok) {
			res.json().then((data) => {
				console.log(res);
                fillPlacesList();  
			});
		} else {
			alert("Error!");
    	}
  });
}

async function fillPlacesList() {
    let token = localStorage.token;
    let guideId = localStorage.id;

    let url = baseUrl + '/places/guide/' + guideId;

    let response = await fetch(url, { 
        method: 'GET',
        headers: { 'Content-Type': 'application/json;charset=utf-8',
                    'Authorization': 'Bearer: ' + token }
    })
    .catch(error => {
        alert(error.message);
    });

    let result = await response.json().catch(error => alert(error.message));
    
    var placeList = result;
    var cardsholder = document.getElementById("cardsholder");
    
    var listSize = placeList.length;
    var htmlString = `<div id="new_place"><button id="new_place_button" class="my-button" onClick="createPlace()"></button></div>`;

    for (i = 0; i < listSize; i++) {
        let id = placeList[i].id;
        let placeName = placeList[i].place_name;
        let cityName = placeList[i].city_name;
        let latitude = placeList[i].latitude;
        let longitude = placeList[i].longitude;
        let visits = placeList[i].visits;
        let long_visits = placeList[i].long_visits;
        let statistics_visits = ``;
        let statistics_long_visits = ``; 
        
        if (visits == null) {
            if (localStorage.getItem('local') == 'EN') {
                statistics_visits = `This place was not visited yet.`;
            } else if (localStorage.getItem('local') == 'UA') {
                statistics_visits = `Це місце ще не відвідав жоден турист.`;
            }
        } else {
            if (localStorage.getItem('local') == 'EN') {
                statistics_visits = `This place was visited ${visits} time(s)`
            } else if (localStorage.getItem('local') == 'UA') {
                statistics_visits = `Це місце було відвідано ${visits} раз(ів).`;
            }
            if (long_visits == null) {
                if (localStorage.getItem('local') == 'EN') {
                    statistics_visits = `But there was no one long visit.`;
                } else if (localStorage.getItem('local') == 'UA') {
                    statistics_visits = `Проте жоден турист ще не прослухав повну екскурсію.`;
                }
            } else {
                if (localStorage.getItem('local') == 'EN') {
                    statistics_long_visits = `And there was ${long_visits} long visit(s).`;
                } else if (localStorage.getItem('local') == 'UA') {
                    statistics_long_visits = `Повну екскурсію було прослухано ${long_visits} раз(ів).`;
                }
            }
        }

        let currentString = `<div class="card my-card" id="placeCard${id}">` +
        `<h5 class="card-header" id="name${id}">${placeName}</h5>` +
        `<div class="card-body">` +
          `<div style="float:left">` +
            `<p class="card-text  my-city-label">City: <text id="city${id}">${cityName}</text></p>` +
            `<a href="#" class="btn btn-primary my-button" id="button${id}" onClick="editPlace(${id})">Edit place</a>` +
          `</div>` +
          `<div id="statistics${id}" style="float:right">` +
            `<p class="card-text my-statistics-label">Statistics:</p>` +
            `<p class="card-text">${statistics_visits}</p>` +
            `<p class="card-text">${statistics_long_visits}</p>` +
          `</div>` +
        `</div>` +
      `</div>`;

        htmlString += currentString;

        var marker = new google.maps.Marker({
            position: {lat: latitude, lng: longitude},
            map: gMap,
            title: placeName
          });

        mapMarkers.push(marker);
    }

    cardsholder.innerHTML = htmlString;
}