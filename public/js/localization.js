if(localStorage.getItem('local') == 'UA'){
	$("#locale").prop('value', 'UA');
	changeLng('UA');
}else if(localStorage.getItem('local') == 'EN'){
	$("#locale").prop('value', 'EN');
	changeLng('EN');
}

function setLocale(lang){
	switch (lang) {
		case 'UA':
			localStorage.setItem('local', 'UA');
			changeLng('UA');
			break;
		case 'EN':
			localStorage.setItem('local', 'EN');
			changeLng('EN');
			break;
	}
}

function changeLng(lng){
	if(lng == 'EN'){
		$("#emailLabel").text('Email address');
		$("#passwordLabel").text('Password');
		$('#nameLabel').text('Name');
		$("#loginButton").text('Log In');
		$('#signupButton').text('Sign Up')
		$('#goToSignUp').text('I have no account yet. Sign Up');
		$('#goToLogIn').text('I have already an account. Log in');

		$('#nav_home').text('Home');
		$('#nav_manage').text('Manage DB');
		$('#navbarDropdown_lng').text('Language');
		$('#nav_logout').text('Log Out');
		$('.my-city-label').text('City');
		$('.my-statistics-label').text('Statistics');

	}else if(lng == 'UA'){
		$("#emailLabel").text('Електронна адреса');
		$("#passwordLabel").text('Пароль');
		$('#nameLabel').text("Ім'я");
		$("#loginButton").text('Увійти');
		$('#signupButton').text('Зареєструватись')
		$('#goToSignUp').text('Я не маю акаунт. Зареєструватись');
		$('#goToLogIn').text('Я вже маю акаунт. Увійти');

		$('#nav_home').text('Головна');
		$('#nav_manage').text('Керувати БД');
		$('#navbarDropdown_lng').text('Мова');
		$('#nav_logout').text('Вийти');
		$('.my-city-label').text('Місто');
		$('.my-statistics-label').text('Статистика');
	}
}