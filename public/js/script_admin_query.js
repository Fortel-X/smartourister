let baseUrl = "https://smartourister.herokuapp.com";

let sendQuery = async () => {
    let query = document.getElementById('query').value;
    let url = baseUrl + '/places/query';
    let token = localStorage.token;

    let response = await fetch(url, { 
        method: 'POST',
        body: JSON.stringify({ query: query }),
        headers: { 'Content-Type':  'text/plain',
                    'Authorization': 'Bearer: ' + token }
    })
    .catch(error => {
        alert(error.message);
    });

    let result = await response.json().catch(error => alert(error.message));
    document.getElementById("query_res").innerText = JSON.stringify(result);
}

function cleardiv(cur_id, cur_el){
    var el = document.getElementById(cur_el);
    document.getElementById(cur_id).innerText = "";
    el.style.color = '#495464';
    el.style.borderColor = 'rgba(73, 84, 100, 0.4)';
  }