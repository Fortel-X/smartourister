package com.fortel_x.android.smartourister.model;

import android.content.Context;

import com.fortel_x.android.smartourister.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Tourist {

    @SerializedName("id")
    @Expose
    private String mId;
    @SerializedName("name")
    @Expose
    private String mName;
    @SerializedName("email")
    @Expose
    private String mEmail;
    @SerializedName("password")
    @Expose
    private String mPassword;
    @SerializedName("paid_until_date")
    @Expose
    private String mPaidUntilDate; //YYYY-MM-DD
    @SerializedName("token")
    @Expose
    private String mToken;

    private static Tourist mInstance;

    private Tourist() {}

    public static Tourist getInstance() {
        if(mInstance == null) {
            mInstance = new Tourist();
        }
        return mInstance;
    }

    public String getId() {
        return mId;
    }

    public Tourist setId(String id) {
        mId = id;
        return mInstance;
    }

    public String getName() {
        return mName;
    }

    public Tourist setName(String name) {
        mName = name;
        return mInstance;
    }

    public String getEmail() {
        return mEmail;
    }

    public Tourist setEmail(String email) {
        mEmail = email;
        return mInstance;
    }

    public String getPassword() {
        return mPassword;
    }

    public Tourist setPassword(String password) {
        mPassword = password;
        return mInstance;
    }

    public String getPaidUntilString() {
        return mPaidUntilDate;
    }

    public Tourist setPaidUntilDate(String paidUntilDate) {
        int dateLength = 10;
        if (paidUntilDate != null && paidUntilDate.length() > dateLength) {
            paidUntilDate = paidUntilDate.substring(0, dateLength);
        }
        mPaidUntilDate = paidUntilDate;
        return mInstance;
    }

    public Date getPaidUntilDate(Context context) {
        String pattern = context.getString(R.string.database_date_format);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date = null;

        if (mPaidUntilDate != null) {
            try {
                date = simpleDateFormat.parse(mPaidUntilDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return date;
    }

    public Tourist setPaidUntilDate(Context context, Date paidUntilDate) {
        String pattern = context.getString(R.string.database_date_format);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        mPaidUntilDate = simpleDateFormat.format(paidUntilDate);
        return mInstance;
    }

    public String getToken() {
        return mToken;
    }

    public Tourist setToken(String token) {
        mToken = token;
        return mInstance;
    }
}
