package com.fortel_x.android.smartourister.ui_controller;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.fortel_x.android.smartourister.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(item -> {
            Fragment selectedFragment = null;

            switch (item.getItemId()) {
                case R.id.nav_home_item:
                    selectedFragment = new HomeFragment();
                    break;
                case R.id.nav_map_item:
                    selectedFragment = new MapFragment();
                    break;
                case R.id.nav_profile_item:
                    selectedFragment = new ProfileFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    selectedFragment).commit();

            return true;
        });

        getSupportFragmentManager().beginTransaction().replace(
                R.id.fragment_container, new HomeFragment()
        ).commit();
        bottomNav.setSelectedItemId(R.id.nav_home_item);
    }
}
