package com.fortel_x.android.smartourister.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Route {

    @SerializedName("id")
    @Expose
    private int mId;
    @SerializedName("name")
    @Expose
    private String mName;

    private List<RoutePoint> mRoutePoints;

    public List<RoutePoint> getRoutePoints() {
        return mRoutePoints;
    }

    public void setRoutePoints(List<RoutePoint> routePoints) {
        mRoutePoints = routePoints;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}