package com.fortel_x.android.smartourister.ui_controller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.fortel_x.android.smartourister.R;
import com.fortel_x.android.smartourister.api.NetworkService;
import com.fortel_x.android.smartourister.model.Tourist;
import com.fortel_x.android.smartourister.utils.InternetConnection;
import com.fortel_x.android.smartourister.utils.Verification;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "SignUpActivity.java";

    private EditText mNameEditText;
    private EditText mEmailEditText;
    private EditText mPasswordEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup);

        mNameEditText = findViewById(R.id.name_edit_text);
        mNameEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyName(this, mNameEditText);
            }
        });

        mEmailEditText = findViewById(R.id.email_edit_text);
        mEmailEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyEmail(this, mEmailEditText);
            }
        });

        mPasswordEditText = findViewById(R.id.password_edit_text);
        mPasswordEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyPassword(this, mPasswordEditText);
            }
        });

        Button signUpButton = findViewById(R.id.sign_up_button);
        signUpButton.setOnClickListener(v -> signUp(
                mNameEditText.getText().toString(),
                mEmailEditText.getText().toString(),
                mPasswordEditText.getText().toString()
        ));

        TextView logInText = findViewById(R.id.log_in_text);
        logInText.setOnClickListener(v -> {
            Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
            startActivity(intent);
        });
    }

    private void signUp(String name, String email, String password) {
        if (!Verification.verifyName(this, mNameEditText)
                || !Verification.verifyEmail(this, mEmailEditText)
                || !Verification.verifyPassword(this, mPasswordEditText)) {
            return;
        } else if (!InternetConnection.checkConnection(getApplicationContext())) {
            Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            return;
        } else {
            Tourist tourist = Tourist.getInstance()
                    .setName(name)
                    .setEmail(email)
                    .setPassword(password);

            NetworkService.getInstance()
                    .getApiService()
                    .touristSignUp(tourist)
                    .enqueue(signupCallback);
        }
    }

    private Callback<Tourist> signupCallback = new Callback<Tourist>() {
        @Override
        public void onResponse(Call<Tourist> call, Response<Tourist> response) {
            if(!response.isSuccessful()) {
                Log.i(TAG, response.message() + " " + response.code());
                Toast.makeText(
                        SignupActivity.this,
                        response.message(),
                        Toast.LENGTH_SHORT
                ).show();
            } else {
                Tourist.getInstance()
                        .setToken(response.body().getToken())
                        .setId(response.body().getId());

                Intent intent = new Intent(SignupActivity.this,
                        MainActivity.class);
                startActivity(intent);
            }
        }

        @Override
        public void onFailure(Call<Tourist> call, Throwable t) {
            Log.i(TAG, t.getMessage());
            Toast.makeText(
                    SignupActivity.this,
                    t.getMessage(),
                    Toast.LENGTH_SHORT
            ).show();
        }
    };
}
