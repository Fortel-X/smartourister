package com.fortel_x.android.smartourister.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;

import com.fortel_x.android.smartourister.R;

import java.util.regex.Pattern;

public class Verification {

    public static boolean verifyName(Context context, EditText input) {
        return true;
    }

    public static boolean verifyEmail(Context context, EditText input) {
        String email = input.getText().toString();
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        if (pattern.matcher(email).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.email_tip));
            return false;
        }
    }

    public static boolean verifyPassword(Context context,EditText input) {
        String password = input.getText().toString();
        Pattern PASSWORD_PATTERN = Pattern.compile("[a-zA-Z0-9]{8,24}");
        if (!TextUtils.isEmpty(password) && PASSWORD_PATTERN.matcher(password).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.password_tip));
            return false;
        }
    }
}
