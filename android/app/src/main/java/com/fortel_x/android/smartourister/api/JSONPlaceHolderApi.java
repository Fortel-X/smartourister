package com.fortel_x.android.smartourister.api;

import com.fortel_x.android.smartourister.model.City;
import com.fortel_x.android.smartourister.model.Route;
import com.fortel_x.android.smartourister.model.RoutePoint;
import com.fortel_x.android.smartourister.model.Tourist;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface JSONPlaceHolderApi {

    @POST("tourists/login")
    Call<Tourist> touristLogIn(@Body Tourist tourist);

    @POST("tourists/signup")
    Call<Tourist> touristSignUp(@Body Tourist tourist);

    @PATCH("tourists/{touristId}")
    Call<Tourist> touristUpdateInfo(
            @Path("touristId") int touristId,
            @Header("Authorization") String token,
            @Body Tourist tourist
    );

    @GET("cities/by_name/{cityName}")
    Call<City> getCity(@Path("cityName") String cityName);

    @GET("routes/city/{cityId}")
    Call<List<Route>> getRoutes(@Path("cityId") int cityId);

    @GET("route_points/route/{routeId}")
    Call<List<RoutePoint>> getRoutePoints(@Path("routeId") int routeId);
}
