package com.fortel_x.android.smartourister.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoutePoint {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("previous_point_id")
    @Expose
    private int previousPointId;
    @SerializedName("next_point_id")
    @Expose
    private int nextPointId;
    @SerializedName("place_id")
    @Expose
    private int placeId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("route_id")
    @Expose
    private int routeId;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
//    @SerializedName("short_audio")
//    @Expose
//    private Blob shortAudio;
//    @SerializedName("detailed_audio")
//    @Expose
//    private Blob detailedAudio;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPreviousPointId() {
        return previousPointId;
    }

    public void setPreviousPointId(int previousPointId) {
        this.previousPointId = previousPointId;
    }

    public int getNextPointId() {
        return nextPointId;
    }

    public void setNextPointId(int nextPointId) {
        this.nextPointId = nextPointId;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

//    public Blob getShortAudio() {
//        return shortAudio;
//    }
//
//    public void setShortAudio(Blob shortAudio) {
//        this.shortAudio = shortAudio;
//    }
//
//    public Blob getDetailedAudio() {
//        return detailedAudio;
//    }
//
//    public void setDetailedAudio(Blob detailedAudio) {
//        this.detailedAudio = detailedAudio;
//    }
}
