package com.fortel_x.android.smartourister.ui_controller;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fortel_x.android.smartourister.R;
import com.fortel_x.android.smartourister.api.NetworkService;
import com.fortel_x.android.smartourister.model.Tourist;
import com.fortel_x.android.smartourister.utils.Verification;

import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    private static final String TAG = "ProfileFragment";

    private TextView mNameTextView;
    private TextView mEmailTextView;
    private EditText mNameEditText;
    private TextView mPaidUntilDateTextView;
    private Button mEditProfileButton;
    private Button mRenewSubscriptionButton;

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        Tourist tourist = Tourist.getInstance();

        mNameTextView = view.findViewById(R.id.name_text_view);
        mNameTextView.setText(tourist.getName());
        mNameEditText = view.findViewById(R.id.name_edit_text);

        mEmailTextView = view.findViewById(R.id.email_text_view);
        mEmailTextView.setText(tourist.getEmail());

        mEditProfileButton = view.findViewById(R.id.edit_profile_button);
        mEditProfileButton.setOnClickListener(v -> editProfile());

       mPaidUntilDateTextView = view.findViewById(R.id.profile_paid_period_label);
       renewPurchasesString();
       mRenewSubscriptionButton = view.findViewById(R.id.renew_subscription_button);
       mRenewSubscriptionButton.setOnClickListener(v -> updateSubscription());

        return view;
    }

    private void editProfile() {
        mNameTextView.setVisibility(View.GONE);
        mNameEditText.setVisibility(View.VISIBLE);
        mNameEditText.setText(Tourist.getInstance().getName());
        mEditProfileButton.setText(R.string.save_changes);
        mEditProfileButton.setOnClickListener(v -> saveChanges());
    }

    private void saveChanges() {
        if(!Verification.verifyName(getContext(), mNameEditText)) {
            return;
        } else {
            if (mNameEditText.getText().toString() != Tourist.getInstance().getName()) {
                Tourist tourist = Tourist.getInstance().setName(mNameEditText.getText().toString());

                NetworkService
                        .getInstance()
                        .getApiService()
                        .touristUpdateInfo(Integer.parseInt(tourist.getId()), tourist.getToken(), tourist)
                        .enqueue(updateSubscriptionCallback);
            }
            mNameTextView.setVisibility(View.VISIBLE);
            mNameEditText.setVisibility(View.GONE);
            mEditProfileButton.setText(R.string.edit_profile);
            mEditProfileButton.setOnClickListener(v -> editProfile());
        }
    }

    private void updateSubscription() {
        Date currentTime = Calendar.getInstance().getTime();
        Date subscribedTime = Tourist.getInstance().getPaidUntilDate(getContext());
        Calendar calendar = Calendar.getInstance();

        if(subscribedTime != null && subscribedTime.compareTo(currentTime) > 0) {
            calendar.setTime(subscribedTime);
        }

        calendar.add(Calendar.DATE, 7);
        Date updatedTime = calendar.getTime();

        Tourist tourist = Tourist.getInstance().setPaidUntilDate(getContext(), updatedTime);

        NetworkService
                .getInstance()
                .getApiService()
                .touristUpdateInfo(Integer.parseInt(tourist.getId()), tourist.getToken(), tourist)
                .enqueue(updateSubscriptionCallback);
    }

    private Callback<Tourist> updateSubscriptionCallback = new Callback<Tourist>() {
        @Override
        public void onResponse(Call<Tourist> call, Response<Tourist> response) {
            if(!response.isSuccessful()) {
                Log.i(TAG, response.message());
                Toast.makeText(
                        getContext(),
                        response.message(),
                        Toast.LENGTH_SHORT
                ).show();
                return;
            }
            mNameTextView.setText(Tourist.getInstance().getName());
            renewPurchasesString();
        }

        @Override
        public void onFailure(Call<Tourist> call, Throwable t) {}
    };

    private void renewPurchasesString() {
        Date date = Tourist.getInstance().getPaidUntilDate(getContext());
        StringBuilder purchasesSb = new StringBuilder();

        if (date == null) {
            purchasesSb.append(getContext().getString(R.string.profile_no_subscription));
        } else {
            purchasesSb.append(getContext().getString(R.string.profile_paid_period_label))
                    .append(" ")
                    .append(Tourist.getInstance().getPaidUntilString());
        }

        mPaidUntilDateTextView.setText(purchasesSb.toString());
    }
}
