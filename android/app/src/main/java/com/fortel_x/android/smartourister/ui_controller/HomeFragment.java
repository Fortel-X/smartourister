package com.fortel_x.android.smartourister.ui_controller;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.fortel_x.android.smartourister.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";
    private static final int REQUEST_CODE = 1;

    private TextView mWelcomeTextView;
    private LinearLayout mStartLinearLayout;
    private LinearLayout mNoPlacesLinearLayout;
    private ImageButton mStartButton;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Location mLocation;
    private MediaPlayer mPlayer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mWelcomeTextView = view.findViewById(R.id.welcome_label);
        mStartLinearLayout = view.findViewById(R.id.start_linear_layout);
        mNoPlacesLinearLayout = view.findViewById(R.id.no_places_nearby_linear_layout);

        mStartButton = view.findViewById(R.id.start_button);
        mStartButton.setOnClickListener(v -> play(v));

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        setCityByLocation(getContext());

        return view;
    }

    private void play(View view) {
        if (mPlayer == null) {
            mPlayer = MediaPlayer.create(getContext(), R.raw.song);
        }

        mPlayer.start();

        mStartButton.setImageResource(R.drawable.ic_pause);
        mStartButton.setOnClickListener(v -> pause(v));
    }

    private void pause(View view) {
        if (mPlayer != null) {
            mPlayer.pause();
        }

        mStartButton.setImageResource(R.drawable.ic_play);
        mStartButton.setOnClickListener(v -> play(v));
    }

    private void setCityByLocation(Context context) {
        if(ActivityCompat.checkSelfPermission(
                getActivity(), Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {
                    Manifest.permission.ACCESS_FINE_LOCATION }, REQUEST_CODE);
            return;
        }
        Task<Location> task = mFusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(location -> {
            if(location != null) {
                mLocation = location;
                try {
                    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                    List<Address> addresses = null;
                    addresses = geocoder.getFromLocation(location.getLatitude(),location.getLongitude(), 1);
                    String cityName = addresses.get(0).getLocality();
                    mWelcomeTextView.setText(context.getString(R.string.welcome_to) + " " + cityName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        switch(requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setCityByLocation(getContext());
                }
                break;
        }
    }
}