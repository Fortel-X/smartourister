package com.fortel_x.android.smartourister.ui_controller;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.fortel_x.android.smartourister.R;
import com.fortel_x.android.smartourister.api.JSONPlaceHolderApi;
import com.fortel_x.android.smartourister.api.NetworkService;
import com.fortel_x.android.smartourister.model.City;
import com.fortel_x.android.smartourister.model.Route;
import com.fortel_x.android.smartourister.model.RoutePoint;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapFragment extends Fragment {

    private static final String TAG = ".MapFragment";
    private static final int REQUEST_CODE = 1;

    private GoogleMap mMap;
    private JSONPlaceHolderApi mApi;
    private Location mCurrentLocation;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private SupportMapFragment mMapFragment;
    private MapMarkerIcon mMapMarkerIcon;
    private City mCity;
    private List<Route> mRoutes;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        mMapMarkerIcon = new MapMarkerIcon();
        mCity = new City();

        mApi = NetworkService.getInstance().getApiService();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        fetchLastLocation();

        mMapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);

        return view;
    }

    private void fetchLastLocation() {
        if(ActivityCompat.checkSelfPermission(
                getActivity(), Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {
                    Manifest.permission.ACCESS_FINE_LOCATION }, REQUEST_CODE);
            return;
        }

        Task<Location> task = mFusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(location -> {
            if(location != null) {
                mCurrentLocation = location;
                mMapFragment.getMapAsync(googleMap -> {
                    mMap = googleMap;
                    showLocation(location);
                    getCityByLocation();
                });
            }
        });
    }

    private void showLocation (Location location) {
        LatLng latLng = new LatLng(location.getLatitude(),
                location.getLongitude());

        BitmapDescriptor icon = mMapMarkerIcon.getCurrentLocationIcon(getContext());

        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .title(getString(R.string.current_location))
                .icon(icon);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        mMap.addMarker(markerOptions);
    }

    private void getCityByLocation(){
        try {
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            List<Address> addresses = null;
            addresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude(), 1);
            String cityName = addresses.get(0).getLocality();
            mCity.setName(cityName);

            mApi.getCity(cityName).enqueue(getCityCallback);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Callback<City> getCityCallback = new Callback<City>() {
        @Override
        public void onResponse(Call<City> call, Response<City> response) {
            if(response.isSuccessful()) {
                mCity.setId(response.body().getId());
                mApi.getRoutes(mCity.getId()).enqueue(getRoutesCallback);
            }
        }

        @Override
        public void onFailure(Call<City> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private Callback<List<Route>> getRoutesCallback = new Callback<List<Route>>() {
        @Override
        public void onResponse(Call<List<Route>> call, Response<List<Route>> response) {
            if(response.isSuccessful()) {
                mRoutes = response.body();
                for (Route r: mRoutes) {
                    getRoutePoints(r.getId());
                }
            }
        }

        @Override
        public void onFailure(Call<List<Route>> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void getRoutePoints(int routeId) {
        mApi.getRoutePoints(routeId).enqueue(getRoutePointsCallbak);
    }

    private Callback<List<RoutePoint>> getRoutePointsCallbak = new Callback<List<RoutePoint>>() {
        @Override
        public void onResponse(Call<List<RoutePoint>> call, Response<List<RoutePoint>> response) {
            if(response.isSuccessful()) {
                List<RoutePoint>  routePoints = response.body();
                addPointsToRoute(routePoints);
                for (Route route:mRoutes) {
                    printRoute(route);
                }
            }
        }

        @Override
        public void onFailure(Call<List<RoutePoint>> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void addPointsToRoute(List<RoutePoint> routePoints) {
        int routeId = routePoints.get(0).getRouteId();
        for (Route route: mRoutes) {
            if(route.getId() == routeId) {
                route.setRoutePoints(routePoints);
                break;
            }
        }
    }

    private void printRoute(Route route) {
        List<RoutePoint> routePoints = route.getRoutePoints();

        Polyline polyline = null;
        List<LatLng> latLngList = new ArrayList<>();
        List<Marker> markerList = new ArrayList<>();

        for (int i = 0; i < routePoints.size(); i++) {
            LatLng latLng = new LatLng(Double.parseDouble(routePoints.get(i).getLatitude()),
                    Double.parseDouble(routePoints.get(i).getLongitude()));
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(route.getName())
                    .snippet(routePoints.get(i).getName())
                    .icon(new MapMarkerIcon().getRoutePointIcon(getContext(), route.getId()))
                    .anchor(0.5f, 0.5f);
            Marker marker = mMap.addMarker(markerOptions);
            latLngList.add(latLng);
            markerList.add(marker);
        }

        PolylineOptions polylineOptions = new PolylineOptions()
                .addAll(latLngList);    //.clickable(true); getRouteColor(route.getId())
        polyline = mMap.addPolyline(polylineOptions);
        polyline.setColor(getRouteColor(route.getId()));
    }

    private int getRouteColor(int routeId) {
        int color;
        switch (routeId % 4) {
            case 1:
                color = Color.rgb(68,175,121);
                break;
            case 2:
                color = Color.rgb(72,98,133);
                break;
            case 3:
                color = Color.rgb(242,220,93);
                break;
            default:
                color = Color.rgb(242,163,89);
                break;
        }
        return color;
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        switch(requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLastLocation();
                }
                break;
        }
    }

    private class MapMarkerIcon {

        private BitmapDescriptor currentLocationIcon;

        public BitmapDescriptor getCurrentLocationIcon(Context context) {
            if(currentLocationIcon == null) { createCurrentLocationIcon(context); }
            return currentLocationIcon;
        }

        public BitmapDescriptor getRoutePointIcon(Context context, int routeId) {
            @DrawableRes int resource;

            switch(routeId % 4) {
                case 1:
                    resource = R.drawable.ic_route_point_green;
                    break;
                case 2:
                    resource = R.drawable.ic_route_point_blue;
                    break;
                case 3:
                    resource = R.drawable.ic_route_point_yellow;
                    break;
                default:
                    resource = R.drawable.ic_route_point_orange;
                    break;
            }
            Drawable drawable = ContextCompat.getDrawable(context, resource);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.draw(canvas);
            return BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        private void createCurrentLocationIcon(Context context) {
            Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_my_location_background);
            background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
            Drawable vectorDrawable = ContextCompat.getDrawable(context, R.drawable.ic_my_location);
            vectorDrawable.setBounds(18, 18, vectorDrawable.getIntrinsicWidth() + 18, vectorDrawable.getIntrinsicHeight() + 18);
            Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            background.draw(canvas);
            vectorDrawable.draw(canvas);
            currentLocationIcon = BitmapDescriptorFactory.fromBitmap(bitmap);
        }
    }
}
