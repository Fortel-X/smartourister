const express = require('express');
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const jsonParser = express.json();

const RoutPointController = require("../controllers/route_point");

router.post("/add", jsonParser, RoutPointController.point_add);

router.get("/:routePointId", checkAuth, RoutPointController.point_get);

router.get("/route/:routeId", RoutPointController.point_get_route);

router.get("/:latitude/:longitude", RoutPointController.point_get_by_location);

module.exports = router;