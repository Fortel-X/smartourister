const express = require('express')
const router = express.Router();
const jsonParser = express.json();

const PlaceController = require("../controllers/planned_place");

router.post("/add", jsonParser, PlaceController.planned_place_add);

router.get("/city/:cityId", PlaceController.places_in_the_city_get);

module.exports = router;