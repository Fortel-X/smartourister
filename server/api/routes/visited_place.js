const express = require('express')
const router = express.Router();
const jsonParser = express.json();

const VisitedPlaceController = require("../controllers/visited_place");

router.post("/add", jsonParser, VisitedPlaceController.visited_place_add);

router.get("/:placeId", VisitedPlaceController.visited_place_get);

module.exports = router;