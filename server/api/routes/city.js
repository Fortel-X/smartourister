const express = require('express')
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const jsonParser = express.json();

const CityController = require("../controllers/city");

router.post("/add", jsonParser, CityController.city_add);

router.get("/:cityId", CityController.city_get);

router.get("/", CityController.city_get_all);

router.get("/by_name/:cityName", CityController.city_get_by_name);

module.exports = router;