const express = require('express')
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const jsonParser = express.json();

const GuideController = require("../controllers/guide");

router.post("/signup", jsonParser, GuideController.guide_signUp);

router.post("/login", jsonParser, GuideController.guide_logIn);

router.get("/:guideId", checkAuth, GuideController.guide_get);

router.patch('/:guideId', checkAuth, GuideController.guide_update);

module.exports = router;

/*
signUp
signIn
get profile
chenge info in profile
*delete user(if he doesn't have current rent)
*/