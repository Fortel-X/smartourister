const express = require('express')
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const jsonParser = express.json();

const RouteController = require("../controllers/route");

router.post("/add", jsonParser, RouteController.route_add);

router.get("/:routeId", RouteController.route_get);

router.get("/city/:cityId", RouteController.routes_in_the_city_get);

router.patch('/:routeId', checkAuth, RouteController.route_update);

module.exports = router;