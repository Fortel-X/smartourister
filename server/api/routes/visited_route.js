const express = require('express')
const router = express.Router();
const jsonParser = express.json();

const VisitedRouteController = require("../controllers/visited_route");

router.post("/add", jsonParser, VisitedRouteController.visited_route_add);

router.get("/:routeId", VisitedRouteController.visited_route_get);

module.exports = router;