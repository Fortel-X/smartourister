const express = require('express')
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const jsonParser = express.json();

const PlaceController = require("../controllers/place");

router.post("/add", jsonParser, PlaceController.place_add);

router.get("/", PlaceController.place_get_all);

router.get("/:placeId", PlaceController.place_get);

router.get("/city/:cityId", PlaceController.places_in_the_city_get);

router.get("/guide/:guideId", PlaceController.places_by_guide_get);

router.patch('/:placeId', checkAuth, jsonParser, PlaceController.place_update);

router.post('/query', checkAuth, jsonParser, PlaceController.place_query);

module.exports = router;