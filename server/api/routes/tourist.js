const express = require('express')
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const jsonParser = express.json();

const TouristController = require("../controllers/tourist");

router.post("/signup", jsonParser, TouristController.tourist_signUp);

router.post("/login", jsonParser, TouristController.tourist_logIn);

router.get("/:touristId", checkAuth, TouristController.tourist_get);

router.patch('/:touristId', jsonParser, checkAuth, TouristController.tourist_update);

module.exports = router;

/*
signUp
signIn
get profile
chenge info in profile
*/