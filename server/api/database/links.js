const Sequelize = require("sequelize");

const Tourist = require('../models/tourist');
const Guide = require('../models/guide');
const Route = require('../models/route');
const RoutePoint = require('../models/route_point.js');
const Place = require('../models/place');
const City = require('../models/city.js');
const VisitedPlace = require('../models/visited_place');
const VisitedRout = require('../models/visited_route');
const PlannedPlace = require('../models/planned_place');
const VisitedPlannedPlace = require('../models/visited_planned_place');

module.exports = () => {
    Tourist.hasMany(VisitedPlannedPlace, {onDelete: "cascade", foreignKey: 'tourist_id'});
    Tourist.hasMany(VisitedRout, {onDelete: "cascade", foreignKey: 'tourist_id'});
    Tourist.hasMany(VisitedPlace, {onDelete: "cascade", foreignKey: 'tourist_id'});
    Guide.hasMany(Place, {onDelete: "cascade", foreignKey: 'guide_id'});
    Route.hasMany(VisitedRout, {onDelete: "cascade", foreignKey: 'route_id'});
    Route.hasMany(RoutePoint, {onDelete: "cascade", foreignKey: 'route_id'});
    Place.hasMany(RoutePoint, {onDelete: "cascade", foreignKey: 'place_id'});
    Place.hasMany(VisitedPlace, {onDelete: "cascade", foreignKey: 'place_id'});
    City.hasMany(Place, {onDelete: "cascade", foreignKey: 'city_id'});
    PlannedPlace.hasMany(VisitedPlannedPlace, {onDelete: "cascade", foreignKey: 'planned_place_id'});
}