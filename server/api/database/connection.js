const Sequelize = require("sequelize");

const sequelize = new Sequelize(
    "heroku_d43109f7aa06794", //database name /smartourister_local_db
    "bff06f17791084", // user name /root
    "ad508075", // password /rgb5185139
    {
    dialect: "mysql",
    host: "eu-cdbr-west-03.cleardb.net", // localhost
    define: {
        timestamps: false
    }
});

// ...sync({ force: true })... to reset db
sequelize.sync({}).then(console.log("Synchronize")).catch((err) => {
    console.log(err);
});

module.exports = sequelize;
global.sequelize = sequelize;