const Sequelize = require("sequelize");
require("../database/connection");
module.exports = sequelize.define("route_point", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    previous_point_id: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    next_point_id: {
        type: Sequelize.INTEGER,
        allowNull: true
    }
});