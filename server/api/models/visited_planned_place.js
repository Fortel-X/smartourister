const Sequelize = require("sequelize");
require("../database/connection");
module.exports = sequelize.define("visited_planned_place", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    date: {
        type: Sequelize.DATE,
        allowNull:false
    }
});