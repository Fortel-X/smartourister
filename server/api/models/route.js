const Sequelize = require("sequelize");
require("../database/connection");
module.exports = sequelize.define("route", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    }
});