const Sequelize = require("sequelize");
require("../database/connection");
module.exports = sequelize.define("planned_place", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    location: {
        type: Sequelize.STRING,
        allowNull: false
    }
});