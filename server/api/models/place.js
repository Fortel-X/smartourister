const Sequelize = require("sequelize");
require("../database/connection");
module.exports = sequelize.define("place", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    latitude: {
        type: Sequelize.FLOAT,
        allowNull: false
    },
    longitude: {
        type: Sequelize.FLOAT,
        allowNull: false
    },
    short_audio: {
        type: Sequelize.BLOB,
        allowNull: false
    },
    detailed_audio: {
        type: Sequelize.BLOB,
        allowNull: true
    }
});