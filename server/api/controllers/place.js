const Place = require('../models/place');
const jwt = require("jsonwebtoken");

exports.place_add = (req, res, next) => {
    const latitude = req.body.latitude;
    const longitude = req.body.longitude;
    const short_audio = req.body.short_audio;
    const detailed_audio = req.body.detailed_audio;
    const city_id = req.body.city_id;
    const name = req.body.name;
    const guide_id = req.body.guide_id;

    Place.create({
        latitude: latitude,
        longitude: longitude,
        short_audio: short_audio,
        detailed_audio: detailed_audio,
        city_id: city_id,
        guide_id: guide_id,
        name: name
    }).then(result => {
        res.status(201).json({ message: "Place created!" });
    }).catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
};

exports.place_get_all = (req, res, next) => {
    const query = `select t3.id, t3.place_name, t3.latitude, t3.longitude, t3.city_name, t3.visits, t4.long_visits from (select t1.id, t1.place_name, t1.latitude, t1.longitude, t1.city_name, t2.visits, t2.place_id from (select P.id, P.name place_name, P.latitude, P.longitude, C.name AS city_name from places P, cities C where P.city_id=C.id) t1 left join (select count(V.id) as visits, V.place_id from visited_places V, places P where V.place_id=P.id group by V.place_id) as t2 on t1.id=t2.place_id) as t3 left join  (select count(V.id) as long_visits, V.place_id from visited_places V, places P where V.place_id=P.id and duration_long='1' group by V.place_id) as t4 on t3.id=t4.place_id;`;
        
    sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    .then(places => {
        if (places == null) {
            res.status(409).json({
                message: "There is no such places."
            });        
        } else {
            res.status(200).json(places);
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
}

exports.place_get = (req, res, next) => {
    const placeId = req.params.placeId;

    Place.findByPk(placeId)
    .then(place => {
        res.status(200).json({
            message: "Place is found.",
            latitude: latitude,
            longitude: longitude,
            short_audio: place.short_audio,
            detailed_audio: place.detailed_audio,
            city_id: city_id
        })
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
};

exports.places_in_the_city_get = (req, res, next) => {
    const cityId = req.params.cityId;
    const query = "select id, latitude, longitude from places where city_id=" + cityId + ";"
    
    sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    .then(places => {
        if (places == null) {
            res.status(409).json({
                message: "There is no such places."
            });        
        } else {
            res.status(200).json(places);
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
};

exports.places_by_guide_get = (req, res, next) => {
    const guideId = req.params.guideId;
    
    const query = `select t3.id, t3.place_name, t3.latitude, t3.longitude, t3.city_name, t3.visits, t4.long_visits from (select t1.id, t1.place_name, t1.latitude, t1.longitude, t1.city_name, t2.visits, t2.place_id from (select P.id, P.name place_name, P.latitude, P.longitude, C.name AS city_name from places P, cities C where P.city_id=C.id and guide_id=${guideId}) t1 left join (select count(V.id) as visits, V.place_id from visited_places V, places P where V.place_id=P.id and P.guide_id=${guideId} group by V.place_id) as t2 on t1.id=t2.place_id) as t3 left join  (select count(V.id) as long_visits, V.place_id from visited_places V, places P where V.place_id=P.id and P.guide_id=${guideId} and duration_long='1' group by V.place_id) as t4 on t3.id=t4.place_id;`;
        
    sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    .then(places => {
        if (places == null) {
            res.status(409).json({
                message: "There is no such places."
            });        
        } else {
            res.status(200).json(places);
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
};

exports.place_update = (req, res, next) => {
    const placeId = req.params.placeId;
    const name = req.body.name;
    const latitude = req.body.latitude;
    const longitude = req.body.longitude;
    const short_audio = req.body.short_audio;
    const detailed_audio = req.body.detailed_audio;

    var updateObject = {};
   
    if (name != null) { updateObject.name = name; }
    if (latitude != null) { updateObject.latitude = latitude; }
    if (longitude != null) { updateObject.latitude = longitude; }
    if (short_audio != null) { updateObject.short_audio = short_audio; }
    if (detailed_audio != null) { updateObject.detailed_audio = detailed_audio; }

    console.log(name);
    console.log(req.body);
    console.log(updateObject);

    jwt.verify(req.token, "secret", (err, authData) => {
        if (err) {
            console.log(err);
            res.status(403).json({
                message: "Wrong token!!!"
            });
        } else {
            Place.update(updateObject, {where: {id: placeId}})
            .then(() => {
                res.status(201).json({
                    message: "Place is updated"
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({ error: err });
            })
        }
    });
};

exports.place_query = (req, res, next) => {
    const query = req.body.query;
        
    sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    .then(places => {
        if (places == null) {
            res.status(409).json({
                message: "There is no such places."
            });        
        } else {
            res.status(200).json(places);
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
}