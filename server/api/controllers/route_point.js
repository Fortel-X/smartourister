const RoutePoint = require('../models/route_point');
const jwt = require("jsonwebtoken");

exports.point_get = (req, res, next) => {
    const pointId = req.params.routePointId;

    jwt.verify(req.token, "secret", (err, authData) => {
        if (err) {
            console.log(err);
            res.status(403).json({
                message: "wrong token!"
            });
        } else {
            Point.findByPk(pointId)
            .then(point => {
                res.status(200).json({
                    message: "Point is found.",
                    place_id: point.place_id,
                    previous_point_id: point.previous_point_id,
                    next_point_id: point.next_point_id,
                    route_id: point.route_id
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({ error: err });
            })
        }
    });
};

exports.point_add = (req, res, next) => {
    const place_id = req.body.place_id;
    const previous_point_id = req.body.previous_point_id;
    const next_point_id = req.body.next_point_id;
    const route_id = req.body.route_id;

    RoutePoint.create({
        place_id: place_id,
        previous_point_id: previous_point_id,
        next_point_id: next_point_id,
        route_id: route_id
    }).then(result => {
        res.status(201).json({ message: "RoutePoint created!" });
    }).catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
};

exports.point_get_route = (req, res, next) => {
    const routeId = req.params.routeId;

    const query = "select R.id, R.next_point_id, R.previous_point_id, R.route_id, P.latitude,"
        + " P.longitude, P.short_audio, P.detailed_audio, P.name from route_points R inner join places"
        + " P on R.place_id=P.id where R.route_id=" + routeId + ";";

    sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    .then(route_points => {
        let orderedPoints = orderPoints(route_points);
        
        res.status(200).json(orderedPoints);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
}

exports.point_get_by_location = (req, res, next) => {
    const latitude = req.params.latitude;
    const longitude = req.params.longitude;

    let latitude_min = latitude - 0.0005;
    let latitude_max = latitude - 0 + 0.0005;
    let longitude_min = longitude - 0.0005;
    let longitude_max = longitude - 0 + 0.0005;

    const query = 'select R.id, R.next_point_id, R.previous_point_id, R.route_id, P.latitude,'
        + ' P.longitude, P.short_audio, P.detailed_audio, P.name from route_points R inner join places'
        + ' P on R.place_id=P.id '
        + 'where latitude between "' + latitude_min + '" and "' + latitude_max + '"'
        + 'and longitude between "' + longitude_min + '" and "' + longitude_max + '";'

    sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    .then(route_points => {

        res.status(200).json(route_points[0]);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
}

function orderPoints (routePoints) {
    let orderedPoints = [];
    let length = routePoints.length;

    if (length == 0) { return; }
    let temp;
    let i = 0;
    let j = 0;
    while(routePoints[j].previous_point_id != null) {
        j++;
    }
    temp = orderedPoints[i] = routePoints[j];
    i++;

    while(temp.next_point_id != null && i < length) {
        let j = 0;
        while(routePoints[j].id != temp.next_point_id) {
            j++;
        }
        temp = orderedPoints[i] = routePoints[j];
        i++;
    }

    return orderedPoints;
}

// async function f(route_point) {    
    // let i = 0;
    // let point = route_point;
    // let points = [];

    // points[i] = point;
    // i++;
    // while (point.next_point_id != null) {
    //     point = await RoutePoint.findByPk(point.next_point_id);
    //     points[i] = point;
    //     i++;
    // }
    // return points;
//}