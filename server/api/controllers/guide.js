const Guide = require('../models/guide');
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const saltRounds = 10; // количество "пробежек" дляя шифрования

exports.guide_signUp = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    const name = req.body.name;

    // SELECT FROM guide
    Guide.findAll({ where: { email: email }, raw: true }) // raw - metadata
    .then(guide => {
        if (guide.length >= 1) {
            return res.status(409).json({
                message: "Mail exists"
            });
        } else {
            bcrypt.hash(password, saltRounds, (err, hash) => {
                if (err) {
                    return res.status(500).json({ error: err });
                } else {
                    Guide.create({
                        name: name,
                        email: email,
                        password: hash
                    }).then(result => {
                        console.log(result);
                        res.status(201).json({
                            message: "Guide created!",
                        });
                    }).catch(err => {
                        console.log(err);
                        res.status(500).json({ error: err });
                    });
                }
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
};

exports.guide_logIn = (req, res, next) => {
    console.log(req.body);
    const email = req.body.email;
    const password = req.body.password;
    console.log(email + " " + password);
    Guide.findAll({where: {email: email }, raw: true})
    .then(guide => {
        if(guide.length < 1) {
            return res.status(401).json({ message: "Oh no! Auth failed!" });
        }
        else {
            bcrypt.compare(password, guide[0].password, (err, result) => {
                if (err) {
                    return res.status(401).json({ message: "Oh no! Auth failed!" });
                } if (result) {
                    const token = jwt.sign({ guide }, "secret", { expiresIn: '10h' });

                    return res.json({
                        message: "LogIn successful;)",
                        token: token,
                        id: guide[0].id
                    })
                }
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
};

exports.guide_get = (req, res, next) => {
    const guideId = req.params.guideId;

    jwt.verify(req.token, "secret", (err, authData) => {
        if (err) {
            console.log(err);
            res.status(403).json({
                message: "Durachok, wrong token!!!"
            });
        } else {
            Guide.findByPk(guideId)
            .then(guide => {
                res.status(200).json({
                    message: "Guide is found.",
                    email: guide.email,
                    name: guide.name,
                    paid_until_date: guide.paid_until_date
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({ error: err });
            })
        }
    });
};

exports.guide_update = (req, res, next) => {
    const guideId = req.params.guideId;
    const name = req.body.name;
    const paid_until_date = req.body.paid_until_date;
    var updateObject = {};

    if (name != null) {
        if (paid_until_date != null) {
            updateObject = {
                name: name,
                paid_until_date: paid_until_date
            }
        } else {
            updateObject = {
                name: name
            }
        }
    } else if (paid_until_date != null) {
        updateObject = {
            paid_until_date: paid_until_date
        }
    }

    jwt.verify(req.token, "secret", (err, authData) => {
        if (err) {
            console.log(err);
            res.status(403).json({
                message: "Wrong token!!!"
            });
        } else {
            Guide.update(updateObject, {where: {id: guideId}})
            .then(() => {
                res.status(201).json({
                    message: "Guide is updated"
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({ error: err });
            })
        }
    });
};