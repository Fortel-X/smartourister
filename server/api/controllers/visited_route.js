const VisitedRoute = require('../models/visited_route');

exports.visited_route_add = (req, res, next) => {
    const route_id = req.body.route_id;
    const tourist_id = req.body.tourist_id;
    const date = req.body.date;
    const duration_long = req.body.duration_long; //true/false
    
    VisitedRoute.create({
        route_id: route_id,
        tourist_id: tourist_id,
        date: date,
        duration_long: duration_long
    }).then(result => {
        res.status(201).json({ message: "VisitedRoute created!" });
    }).catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
};

// Аналіз популярності маршрутів.
exports.visited_route_get = (req, res, next) => {
    const routeId = req.params.routeId;

    const query = "SELECT count(*) AS visits FROM visited_routes WHERE route_id=" + routeId + ";";

    sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    .then((visits) => {
        return res.status(200).json({
            route_id: routeId,
            visits: visits[0].visits
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
}; 