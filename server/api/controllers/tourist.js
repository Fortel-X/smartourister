const Tourist = require('../models/tourist');
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const saltRounds = 10; // количество "пробежек" дляя шифрования

exports.tourist_signUp = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    const name = req.body.name;

    // SELECT FROM tourist
    Tourist.findAll({ where: { email: email }, raw: true }) // raw - metadata
    .then(tourist => {
        if (tourist.length >= 1) {
            return res.status(409).json({
                message: "Mail exists"
            });
        } else {
            bcrypt.hash(password, saltRounds, (err, hash) => {
                if (err) {
                    return res.status(500).json({ error: err });
                } else {
                    Tourist.create({
                        name: name,
                        email: email,
                        password: hash
                    }).then(result => {
                        Tourist.findAll({ where: { email: email }, raw: true })
                        .then(newTourist => {
                            const token = jwt.sign({ tourist }, "secret", { expiresIn: '10h' });

                            res.status(201).json({
                                message: "Tourist created!",
                                token: token,
                                id: newTourist.id
                            });
                        }).catch(err => {
                            console.log(err);
                            res.status(500).json({ error: err });
                        });
                    }).catch(err => {
                        console.log(err);
                        res.status(500).json({ error: err });
                    });
                }
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
};

exports.tourist_logIn = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    Tourist.findAll({ where: { email: email }, raw: true})
    .then(tourist => {
        if(tourist.length < 1) {
            return res.status(401).json({ message: "Oh no! Auth failed!" });
        }
        else {
            bcrypt.compare(password, tourist[0].password, (err, result) => {
                if (err) {
                    return res.status(401).json({ message: "Wrong password" });
                } if (result) {
                    const token = jwt.sign({ tourist }, "secret", { expiresIn: '10h' });

                    return res.json({
                        message: "LogIn successful;)",
                        token: token,
                        id: tourist[0].id,
                        name: tourist[0].name,
                        paid_until_date: tourist[0].paid_until_date
                    })
                }
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
};

exports.tourist_get = (req, res, next) => {
    const touristId = req.params.touristId;

    jwt.verify(req.token, "secret", (err, authData) => {
        if (err) {
            console.log(err);
            res.status(403).json({
                message: "Durachok, wrong token!!!"
            });
        } else {
            Tourist.findByPk(touristId)
            .then(tourist => {
                res.status(200).json({
                    message: "Tourist is found.",
                    email: tourist.email,
                    name: tourist.name,
                    paid_until_date: tourist.paid_until_date
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({ error: err });
            })
        }
    });
};

exports.tourist_update = (req, res, next) => {
    const touristId = req.params.touristId;
    console.log(req.body);
    const name = req.body.name;
    const paid_until_date = req.body.paid_until_date;
    var updateObject = {};
   
    if (name != null) {
        if (paid_until_date != null) {
            updateObject = {
                name: name,
                paid_until_date: paid_until_date
            }
        } else {
            updateObject = {
                name: name
            }
        }
    } else if (paid_until_date != null) {
        updateObject = {
            paid_until_date: paid_until_date
        }
    }

    jwt.verify(req.body.token, "secret", (err, authData) => {
        if (err) {
            console.log(err);
            res.status(403).json({
                message: "Wrong token!!!"
            });
        } else {
            Tourist.update(updateObject, {where: {id: touristId}})
            .then(() => {
                res.status(201).json({
                    message: "Tourist is updated"
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({ error: err });
            })
        }
    });
};