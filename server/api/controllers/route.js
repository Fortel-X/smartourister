const Route = require('../models/route');
const jwt = require("jsonwebtoken");
const Sequelize = require("sequelize");

exports.route_add = (req, res, next) => {
    const name = req.body.name;
    const first_point_id = req.params.first_point_id;

    // SELECT FROM route
    Route.findAll({ where: { name: name }, raw: true }) // raw - metadata
    .then(route => {
        if (route.length >= 1) {
            return res.status(409).json({
                message: "Route exists"
            });
        } else {
            Route.create({
                name: name,
                first_point_id: first_point_id
            }).then(result => {
                res.status(201).json({ message: "Route created!" });
            }).catch(err => {
                console.log(err);
                res.status(500).json({ error: err });
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
};

exports.route_get = (req, res, next) => {
    const routeId = req.params.routeId;

    Route.findByPk(routeId)
    .then(route => {
        res.status(200).json({
            message: "Route is found.",
            name: route.name,
            first_point_id: route.first_point_id
        })
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
};

exports.routes_in_the_city_get = (req, res, next) => {
    const cityId = req.params.cityId;

    const query = "SELECT * FROM routes WHERE routes.id IN (SELECT route_points.route_id FROM route_points "
        + "WHERE place_id IN (SELECT id FROM places WHERE city_id=" + cityId + "));";

    sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    .then((routes) => {
        return res.status(200).json(routes);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
};

exports.route_update = (req, res, next) => {
    const routeId = req.params.routeId;
    const name = req.body.name;
    const first_point_id = req.params.first_point_id;

    var updateObject = {};
   
    if (name != null) { updateObject.location = location; }
    if (first_point_id != null) { updateObject.first_point_id = first_point_id; }

    jwt.verify(req.token, "secret", (err, authData) => {
        if (err) {
            console.log(err);
            res.status(403).json({
                message: "Wrong token!!!"
            });
        } else {
            Route.update(updateObject, {where: {id: routeId}})
            .then(() => {
                res.status(201).json({
                    message: "Route is updated"
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({ error: err });
            })
        }
    });
};