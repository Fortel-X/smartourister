const PlannedPlace = require('../models/planned_place');

exports.planned_place_add = (req, res, next) => {
    const location = req.body.location;
    
    PlannedPlace.create({
        location: location
    }).then(result => {
        res.status(201).json({ message: "PlannedPlace created!" });
    }).catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
};

exports.places_in_the_city_get = (req, res, next) => {
    const cityId = req.params.cityId;
    const query = "select id, latitude, longitude from planned_places where city_id=" + cityId + ";"
    
    sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    .then(places => {
        if (places == null) {
            res.status(409).json({
                message: "There is no such places."
            });        
        } else {
            res.status(200).json(places);
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
};