const VisitedPlace = require('../models/visited_place');

exports.visited_place_add = (req, res, next) => {
    const place_id = req.body.place_id;
    const tourist_id = req.body.tourist_id;
    const date = req.body.date;
    const duration_long = req.body.duration_long; //true/false
    
    VisitedPlace.create({
        place_id: place_id,
        tourist_id: tourist_id,
        date: date,
        duration_long: duration_long
    }).then(result => {
        res.status(201).json({ message: "VisitedPlace created!" });
    }).catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
};

// Аналіз популярності місць, інформації про які, ще немає в системі.
exports.visited_place_get = (req, res, next) => {
    const placeId = req.params.placeId;

    const query = "SELECT count(*) AS visits FROM visited_planned_places WHERE planned_place_id=" + placeId + ";";

    sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    .then((visits) => {
        return res.status(200).json({
            place_id: placeId,
            visits: visits[0].visits
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
}; 