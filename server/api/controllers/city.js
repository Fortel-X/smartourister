const City = require('../models/city');
const jwt = require("jsonwebtoken");

exports.city_add = (req, res, next) => {
    const name = req.body.name;

    // SELECT FROM city
    City.findAll({ where: { name: name }, raw: true }) // raw - metadata
    .then(city => {
        if (city.length >= 1) {
            return res.status(409).json({
                message: "City exists"
            });
        } else {
            City.create({
                name: name
            }).then(result => {
                res.status(201).json({ message: "City created!" });
            }).catch(err => {
                console.log(err);
                res.status(500).json({ error: err });
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
};

exports.city_get = (req, res, next) => {
    const cityId = req.params.cityId;

    City.findByPk(cityId)
    .then(city => {
        res.status(200).json({
            message: "City is found.",
            name: city.name
        })
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
};

exports.city_get_all = (req, res, next) => {
    City.findAll() // raw - metadata
    .then(cities => {
        res.status(201).json({ cities: cities });        
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
}

exports.city_get_by_name = (req, res, next) => {
    const cityName = req.params.cityName;
    
    City.findAll({ where: { name: cityName }, raw: true }) // raw - metadata
    .then(city => {
        if (city.length = 1) {
            res.status(200).json({
                message: "City is found.",
                id: city[0].id
            });
        }
        else {
            res.status(201).json({
                message: "City is not found."
            })
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    })
}