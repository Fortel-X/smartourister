const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = process.env.PORT || 7000;

const placeRoutes = require('./api/routes/place');
const routeRoutes = require('./api/routes/route');
const cityRoutes = require('./api/routes/city');
const routePointRoutes = require('./api/routes/route_point');
const touristRoutes = require('./api/routes/tourist');
const guideRoutes = require('./api/routes/guide');
const visitedPlaceRoutes = require('./api/routes/visited_place');
const plannedPlaceRoutes = require('./api/routes/planned_place');
const visitedPlannedPlaceRoutes = require('./api/routes/visited_planned_place');
const visitedRouteRoutes = require('./api/routes/visited_route');

const links = require('./api/database/links');
links();

let allowCrossDomain = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Headers', "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE");
  next();
}
app.use(allowCrossDomain);

app.use(express.json({
  type: ['application/json', 'text/plain']
}))

app.use('/places', placeRoutes);
app.use('/routes', routeRoutes);
app.use('/cities', cityRoutes);
app.use('/route_points', routePointRoutes);
app.use('/tourists', touristRoutes);
app.use('/guides', guideRoutes);
app.use('/visited_places', visitedPlaceRoutes);
app.use('/planned_places', plannedPlaceRoutes);
app.use('/visited_planned_places', visitedPlannedPlaceRoutes);
app.use('/visited_routes', visitedRouteRoutes);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    const error = Error('Not found');
    error.status = 404;
    next(error);
});

app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        error: {
            message: err.message
        }
    });
});


app.listen(port, function(){
  console.log("Server is running on port " + port);
});